const exec = require('await-exec')
const express = require('express')
const app = express()
const port = 3000

app.get('/', async(req, res) => {
    let cat = await exec('nyancat -f 1 -W 80 -H 80 | aha -b');
    res.send( cat.stdout );
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

